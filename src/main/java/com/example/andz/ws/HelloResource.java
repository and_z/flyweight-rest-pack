package com.example.andz.ws;

import com.example.andz.domain.Greeting;
import com.example.andz.service.HelloService;
import com.example.andz.util.log.InjectLogger;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("hello")
public class HelloResource {

	@InjectLogger
	Logger LOG;

	@Inject
	HelloService service;

	@GET
	public String sayHello() {
		LOG.info("Calling resource method");
		return service.sayHello();
	}

	@GET
	@Path("/greeting")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllGreetings() {
		List<Greeting> greetings = service.getAll();
		return Response.ok(greetings).build();
	}

	@POST
	@Path("/greeting")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createGreeting(Greeting greeting) {
		Greeting newGreeting = service.createNew(greeting);
		return Response.ok(newGreeting).build();
	}
}
