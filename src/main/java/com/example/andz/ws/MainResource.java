package com.example.andz.ws;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("")
public class MainResource {

	@GET
	public String foo(){
		return "I'm the main resource.";
	}
}
