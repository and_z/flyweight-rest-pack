package com.example.andz.servlet;

import com.example.andz.module.HelloModule;
import com.example.andz.module.LoggerModule;
import com.example.andz.module.PersistenceModule;
import com.google.inject.Module;
import org.jboss.resteasy.plugins.guice.GuiceResteasyBootstrapServletContextListener;

import javax.servlet.ServletContext;
import java.util.Arrays;
import java.util.List;

public class ServletContextListener extends GuiceResteasyBootstrapServletContextListener {

	@Override
	protected List<? extends Module> getModules(ServletContext context) {
		return Arrays.asList(new LoggerModule(), new HelloModule(), new PersistenceModule());
	}
}
