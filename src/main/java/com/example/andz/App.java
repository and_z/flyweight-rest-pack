package com.example.andz;

import com.example.andz.servlet.ServletContextListener;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.webapp.WebAppContext;
import org.jboss.resteasy.plugins.server.servlet.HttpServletDispatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {
	static Logger LOG = LoggerFactory.getLogger(App.class);

	public static void main(String[] args) throws Exception {
		Server server = new Server(8080);

		WebAppContext webAppContext = new WebAppContext();
		webAppContext.setResourceBase("./src/main/webapp");
		webAppContext.setContextPath("/");

		webAppContext.addEventListener(new ServletContextListener());

		webAppContext.addServlet(HttpServletDispatcher.class, "/*");
		webAppContext.setServer(server);

		server.setHandler(webAppContext);

		server.start();
		server.join();
	}
}
