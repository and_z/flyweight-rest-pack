package com.example.andz.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Greeting {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long id;

	public String name;
	public String greet;

	public Greeting() {}

	public Greeting(String name, String greet) {
		this.name = name;
		this.greet = greet;
	}
}
