package com.example.andz.util.log;

import com.google.inject.TypeLiteral;
import com.google.inject.spi.TypeEncounter;
import com.google.inject.spi.TypeListener;
import org.slf4j.Logger;

import java.lang.reflect.Field;

public class SLF4JTypeListener implements TypeListener {
	@Override
	public <I> void hear(TypeLiteral<I> type, TypeEncounter<I> encounter) {
		for (Field field : type.getRawType().getDeclaredFields()) {
			if(Logger.class == field.getType() && field.isAnnotationPresent(InjectLogger.class))
				encounter.register(new SLF4JInjector<I>(field));
		}
	}
}
