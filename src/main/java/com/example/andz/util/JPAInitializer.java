package com.example.andz.util;

import com.google.inject.persist.PersistService;

import javax.inject.Inject;

public class JPAInitializer {

	@Inject
	public JPAInitializer(PersistService persistService) {
		persistService.start();
	}
}
