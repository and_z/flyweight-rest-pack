package com.example.andz.service;

import com.example.andz.domain.Greeting;
import com.example.andz.util.log.InjectLogger;
import com.google.inject.persist.Transactional;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class HelloService {

	@InjectLogger
	Logger LOG;

	@Inject
	EntityManager em;

	public String sayHello() {
		LOG.info("Saying hello...");
		return "Hello";
	}

	@Transactional
	public Greeting createNew(Greeting greeting) {
		em.persist(greeting);
		return greeting;
	}

	public List<Greeting> getAll() {
		TypedQuery<Greeting> query = em.createQuery("select g from Greeting g", Greeting.class);
		return query.getResultList();
	}
}