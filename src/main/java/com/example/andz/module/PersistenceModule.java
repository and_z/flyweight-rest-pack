package com.example.andz.module;

import com.example.andz.util.JPAInitializer;
import com.google.inject.AbstractModule;
import com.google.inject.persist.jpa.JpaPersistModule;

public class PersistenceModule extends AbstractModule {

	public static final String PERSISTENCE_UNIT = "hsqldb-mem-unit";

	@Override
	protected void configure() {
		install(new JpaPersistModule(PERSISTENCE_UNIT));
		bind(JPAInitializer.class).asEagerSingleton();
	}
}
