package com.example.andz.module;

import com.example.andz.service.HelloService;
import com.example.andz.ws.HelloResource;
import com.example.andz.ws.MainResource;
import com.google.inject.persist.PersistFilter;
import com.google.inject.servlet.ServletModule;

public class HelloModule extends ServletModule {
	@Override
	protected void configureServlets() {

		filter("/*").through(PersistFilter.class);

		bind(MainResource.class);
		bind(HelloResource.class);

		bind(HelloService.class);

	}
}
