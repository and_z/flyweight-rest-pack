package com.example.andz.module;

import com.example.andz.util.log.SLF4JTypeListener;
import com.google.inject.AbstractModule;
import com.google.inject.matcher.Matchers;

public class LoggerModule extends AbstractModule {
	@Override
	protected void configure() {
		bindListener(Matchers.any(), new SLF4JTypeListener());
	}
}
